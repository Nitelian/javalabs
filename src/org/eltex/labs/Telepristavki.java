package org.eltex.labs;

import org.eltex.labs.Good;

import java.util.Scanner;

public class Telepristavki extends Good {

    String modelp;
    String modelsp[] = {"M1", "M2", "M3", "M4"};


   public void create(){
       super.create();

       int indexmp = (int)Math.random()*3;
       modelp = modelsp[indexmp];

    }

    public void read(){
        super.read();

        System.out.println("Модель: " + modelp);

    }

    public void update(){
        super.update();

        Scanner sc = new Scanner(System.in);
        System.out.println("Введите модель");
        modelp = sc.next();

    }

    public void delete(){
        super.delete();
        modelp = "";
    }


}
