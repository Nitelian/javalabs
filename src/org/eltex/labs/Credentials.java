package org.eltex.labs;

import java.util.UUID;

public class Credentials {


    private UUID Id;
    private String famili;
    private String name;
    private String otchestvo;
    private String email;



    public Credentials(String famili, String name, String otchestvo, String email) {
        Id = UUID.randomUUID();
        this.famili = famili;
        this.name = name;
        this.otchestvo = otchestvo;
        this.email = email;
    }

    public Credentials() {

    }

    public UUID getId() {
        return Id;
    }

    public String getFamili() {
        return famili;
    }

    public void setFamili(String famili) {
        this.famili = famili;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtchestvo() {
        return otchestvo;
    }

    public void setOtchestvo(String otchestvo) {
        this.otchestvo = otchestvo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
