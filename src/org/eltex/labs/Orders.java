package org.eltex.labs;

import java.util.PriorityQueue;

public class Orders {

    ShoppingCart cart = new ShoppingCart();
    Credentials credential = new Credentials();
    Order order = new Order(cart, credential);
    StatusZakaza status = StatusZakaza.PENDING;


    PriorityQueue<Order> priorityQueueZakazy = new PriorityQueue<>();

    public void oformleniePokupki(ShoppingCart cart, Credentials credential) {

        Order order = new Order(cart, credential);
        priorityQueueZakazy.add(order);

    }


    public void CheckOrdersQueue() {
        //разобраться с обходом коллекции
        for (Order order : priorityQueueZakazy) {
            if (status == StatusZakaza.PENDING && order.checkTime()) {
                priorityQueueZakazy.remove(order);
            }
        }
    }


    //------------------------------
    //14) ф-ия показать все заказы
    public void ShowOrders(){
        Object[] arr = priorityQueueZakazy.toArray();

        for ( int i = 0; i<arr.length; i++ ) {
            System.out.println("Value: " + arr[i].toString());
        }
    }


}
