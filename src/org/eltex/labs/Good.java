package org.eltex.labs;

import java.util.Scanner;
import java.util.UUID;

abstract class Good implements ICrudAction {


    public Good() {

    }

    UUID ID;
    String name;
    int price;
    static int count;
    String firm;
    String model;

    int ArrPrice[] = {180000, 6180, 20000, 3000000, 60000};
    String names[] = {"Galaxy", "Andromeda", "Mars"};
    String firms[] = {"Firm1", "Firm2", "Firm3", "Firm4"};


    public void create() {
        int index = (int) Math.random() * 2;
        name = names[index];

        count++;

        int indexX = (int) Math.random() * 4;
        price = ArrPrice[indexX];

        int indexf = (int) Math.random() * 3;
        firm = firms[indexf];

        ID = UUID.randomUUID(); /*5*/
    }

    public void read() {
        System.out.println("ID: " + ID);
        System.out.println("Название: " + name);
        System.out.println("Фирма: " + firm);
        System.out.println("Цена: " + price);
        System.out.println("Количество: " + count);
    }

    public void update() {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите название ");
        name = sc.next();
        System.out.println("Введите фирму");
        firm = sc.next();
        System.out.println("Введите цену");
        price = sc.nextInt();
       // System.out.println("Введите ID");

        //sc.close();
    }

    public void delete() {
        name = "";
        price = 0;
        firm = "";
        count--;
    }


}
